package throttle

import (
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"time"
)


// A throttler processes user commands which are spaced
// via a customizable interval. This means: two commands
// are being processed with a delay between both of a
// fixed specified time.
type Throttler struct {
	lapse float64
	locked bool
}


// Creates a new throttler.
func NewThrottler(lapse float64) *Throttler {
	return &Throttler{values.MaxF64(lapse, 0.000000001), false}
}


// Attempting to run a command inside a throttler may
// involve failing (if the throttler is inside a
// throttle time) or succeeding: actually running the
// command and then entering in throttle time.
func (throttler *Throttler) Run(target func()) bool {
	if !throttler.locked {
		throttler.locked = true
		target()
		go (func(){
			time.Sleep(time.Duration(throttler.lapse * 1000000000.0))
			throttler.locked = false
		})()
		return true
	} else {
		return false
	}
}
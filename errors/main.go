package errors

import "fmt"


// Base GMM errors have a typename and some details.
type GMMError struct {
	typename string
 	details  string
}


// Returns the typename of this error.
func (gmmError GMMError) Typename() string {
	return gmmError.typename
}


// Returns the detail of this error.
func (gmmError GMMError) Details() string {
	return gmmError.details
}


// Returns the full message error in compliance to the
// error interface.
func (gmmError GMMError) Error() string {
	return fmt.Sprintf(
		"%s: %s",
		gmmError.typename, gmmError.details,
	)
}


// Creates a GMM error.
func GMM(typename, details string) GMMError {
	return GMMError{typename, details}
}
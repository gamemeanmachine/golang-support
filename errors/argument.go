package errors

import (
	"fmt"
)


// Argument errors tell when an argument is wrongly populated
// in a call to a function or method.
type ArgumentError struct {
	gmmError GMMError
	function string
	argument string
}


// Creates a new ArgumentError.
func Argument(function, argument, details string) ArgumentError {
	return ArgumentError{
		GMM("ArgumentError", details),
		function,
		argument,
	}
}


// Returns the function where the error occurred.
func (argumentError ArgumentError) Function() string {
	return argumentError.function
}


// Returns the argument to which this error is related.
func (argumentError ArgumentError) Argument() string {
	return argumentError.argument
}


// Returns the full message error in compliance to the
// error interface.
func (argumentError ArgumentError) Error() string {
	return fmt.Sprintf(
		"Argument error while calling %s, for the argument %s: %s",
		argumentError.function, argumentError.argument, argumentError.gmmError.Details(),
	)
}
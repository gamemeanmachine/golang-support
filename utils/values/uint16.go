package values


func MaxU16(a, b uint16) uint16 {
	if a > b {
		return a
	} else {
		return b
	}
}


func MinU16(a, b uint16) uint16 {
	if a < b {
		return a
	} else {
		return b
	}
}


func ClampU16(min, value, max uint16) uint16 {
	if value < min {
		return min
	} else if value > max {
		return max
	} else {
		return value
	}
}


func InU16(min, value, max uint16) bool {
	return value >= min && value <= max
}
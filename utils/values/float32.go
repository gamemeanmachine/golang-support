package values


func MaxF32(a, b float32) float32 {
	if a > b {
		return a
	} else {
		return b
	}
}


func MinF32(a, b float32) float32 {
	if a < b {
		return a
	} else {
		return b
	}
}


func ClampF32(min, value, max float32) float32 {
	if value < min {
		return min
	} else if value > max {
		return max
	} else {
		return value
	}
}


func InF32(min, value, max float32) bool {
	return value >= min && value <= max
}
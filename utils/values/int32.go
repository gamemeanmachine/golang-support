package values


func MaxI32(a, b int32) int32 {
	if a > b {
		return a
	} else {
		return b
	}
}


func MinI32(a, b int32) int32 {
	if a < b {
		return a
	} else {
		return b
	}
}


func ClampI32(min, value, max int32) int32 {
	if value < min {
		return min
	} else if value > max {
		return max
	} else {
		return value
	}
}


func InI32(min, value, max int32) bool {
	return value >= min && value <= max
}
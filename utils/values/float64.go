package values


func MaxF64(a, b float64) float64 {
	if a > b {
		return a
	} else {
		return b
	}
}


func MinF64(a, b float64) float64 {
	if a < b {
		return a
	} else {
		return b
	}
}


func ClampF64(min, value, max float64) float64 {
	if value < min {
		return min
	} else if value > max {
		return max
	} else {
		return value
	}
}


func InF64(min, value, max float64) bool {
	return value >= min && value <= max
}
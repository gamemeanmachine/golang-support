package values


func MaxU8(a, b uint8) uint8 {
	if a > b {
		return a
	} else {
		return b
	}
}


func MinU8(a, b uint8) uint8 {
	if a < b {
		return a
	} else {
		return b
	}
}


func ClampU8(min, value, max uint8) uint8 {
	if value < min {
		return min
	} else if value > max {
		return max
	} else {
		return value
	}
}


func InU8(min, value, max uint8) bool {
	return value >= min && value <= max
}
package from

import "reflect"


// For int, uint, float and complex we need these kind sets. For the complex
// case, it will never be used inside json, but it is still a primitive type.
// For all the composite types, it will not be attended here.


var intKinds = map[reflect.Kind]bool{
	reflect.Int: true, reflect.Int8: true, reflect.Int16: true, reflect.Int32: true, reflect.Int64: true,
}
var lowerIntKinds = map[reflect.Kind]bool{
	reflect.Int8: true, reflect.Int16: true, reflect.Int32: true,
}
var uintKinds = map[reflect.Kind]bool{
	reflect.Uint: true, reflect.Uint8: true, reflect.Uint16: true, reflect.Uint32: true, reflect.Uint64: true,
}
var lowerUintKinds = map[reflect.Kind]bool{
	reflect.Uint8: true, reflect.Uint16: true, reflect.Uint32: true,
}
var floatKinds = map[reflect.Kind]bool{
	reflect.Float32: true, reflect.Float64: true,
}
var complexKinds = map[reflect.Kind]bool{
	reflect.Complex64: true, reflect.Complex128: true,
}


// Tells whether a reflect value is among the specified keys.
func isKind(value reflect.Value, kinds map[reflect.Kind]bool) bool {
	_, ok := kinds[value.Kind()]
	return ok
}


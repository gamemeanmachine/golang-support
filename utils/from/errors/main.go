package errors

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"fmt"
)


// This error is raised when a key is retrieved, but
// it is not of the expected type.
type BadTypeError struct {
	errors.GMMError
	expected string
	actual   string
}


// Returns the message for this error.
func (badTypeError BadTypeError) Error() string {
	return fmt.Sprintf(
		"the value was requested as %s type, but is %s instead",
		badTypeError.expected, badTypeError.actual,
	)
}


// Creates a BadType error.
func BadType(expected, actual string) BadTypeError {
	return BadTypeError{
		errors.GMM("from:bad-kind", ""),
		expected, actual,
	}
}

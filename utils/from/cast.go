package from

import (
	"math"
	"gitlab.com/gamemeanmachine/golang-support/utils/from/errors"
	"reflect"
	"fmt"
	"math/bits"
)


// Castable values are structs with single arbitrary values
// but with methods to get / convert the value to another
// type.
type Cast struct {
	v interface{}
}


// Attempts to cast it as a string. It will succeed, and force a string,
// if the value is of string or a string-like (string-derived) type. It
// will fail if it's not of the same kind (a string-like type). The
// failure is returned as an error instead of panicking.
func (c Cast) AsStringIfPossible() (string, error) {
	if c.v == nil {
		return "", errors.BadType("string-like", "nil")
	}
	value := reflect.ValueOf(c.v)
	if value.Kind() == reflect.String {
		return value.String(), nil
	} else {
		return "", errors.BadType("string-like", value.Type().String())
	}
}


// Attempts to cast it as a string. It will succeed, and force a string,
// if the value is of string or a string-like (string-derived) type. It
// will fail if it's not of the same kind (a string-like type).
func (c Cast) AsString() string {
	if result, err := c.AsStringIfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as a bool. It will succeed, and force a bool value,
// if the value is of boolean or a boolean-like (bool-derived) type. It will
// fail if it's not of the same kind (a boolean-like type). The failure is
// returned as an error instead of panicking.
func (c Cast) AsBoolIfPossible() (bool, error) {
	if c.v == nil {
		return false, errors.BadType("bool-like", "nil")
	}
	value := reflect.ValueOf(c.v)
	if value.Kind() == reflect.Bool {
		return value.Bool(), nil
	} else {
		return false, errors.BadType("bool-like", value.Type().String())
	}
}


// Attempts to cast it as a bool. It will succeed, and force a bool value,
// if the value is of boolean or a boolean-like (bool-derived) type. It will
// fail if it's not of the same kind (a boolean-like type).
func (c Cast) AsBool() bool {
	if result, err := c.AsBoolIfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Internal function to get a float value. This function is internal and
// will always get a float64 value, but the error message will be customized
// accordingly.
func (c Cast) getFloat(size uint) (float64, error) {
	if c.v == nil {
		return 0, errors.BadType(fmt.Sprintf("float%d-like", size), "nil")
	}
	value := reflect.ValueOf(c.v)
	if isKind(value, floatKinds) {
		return value.Float(), nil
	} else if isKind(value, intKinds) {
		return float64(value.Int()), nil
	} else if isKind(value, uintKinds) {
		return float64(value.Uint()), nil
	} else {
		return 0, errors.BadType(fmt.Sprintf("float%d-like", size), value.Type().String())
	}
}


// Internal function to get an int value. This function is internal and
// will always get an int64 value, but the error message will be customized
// accordingly. This function accounts for int/uint being 32 or 64 bits.
func (c Cast) getInt(size uint) (int64, error) {
	if c.v == nil {
		return 0, errors.BadType(fmt.Sprintf("int%d-like", size), "nil")
	}
	value := reflect.ValueOf(c.v)
	if isKind(value, intKinds) {
		return value.Int(), nil
	} else if isKind(value, lowerUintKinds) || bits.UintSize == 32 && value.Kind() == reflect.Uint {
		return int64(value.Uint()), nil
	} else if value.Kind() == reflect.Uint64 || bits.UintSize == 64 && value.Kind() == reflect.Uint {
		uintValue := value.Uint()
		if uintValue <= math.MaxInt64 {
			return int64(uintValue), nil
		} else {
			return 0, errors.BadType(fmt.Sprintf("int%d-like", size), value.Type().String())
		}
	} else if isKind(value, floatKinds) {
		floatValue := value.Float()
		if float64(math.MinInt64) <= floatValue && floatValue <= float64(math.MaxInt64) {
			return int64(floatValue), nil
		} else {
			return 0, errors.BadType(fmt.Sprintf("int%d-like", size), value.Type().String())
		}
	} else if isKind(value, complexKinds) {
		complexValue := value.Complex()
		floatValue := real(complexValue)
		if float64(math.MinInt64) <= floatValue && floatValue <= float64(math.MaxInt64) && imag(complexValue) == 0 {
			return int64(floatValue), nil
		} else {
			return 0, errors.BadType(fmt.Sprintf("uint%d-like", size), value.Type().String())
		}
	} else {
		return 0, errors.BadType(fmt.Sprintf("int%d-like", size), value.Type().String())
	}
}


// Internal function to get an uint value. This function is internal and
// will always get an uint64 value, but the error message will be customized
// accordingly. This function accounts for int/uint being 32 or 64 bits.
func (c Cast) getUint(size uint) (uint64, error) {
	if c.v == nil {
		return 0, errors.BadType(fmt.Sprintf("uint%d-like", size), "nil")
	}
	value := reflect.ValueOf(c.v)
	if isKind(value, uintKinds) {
		return value.Uint(), nil
	} else if isKind(value, lowerIntKinds) || bits.UintSize == 32 && value.Kind() == reflect.Int {
		return uint64(value.Int()), nil
	} else if value.Kind() == reflect.Int64 || bits.UintSize == 64 && value.Kind() == reflect.Int {
		intValue := value.Int()
		if intValue >= 0 {
			return uint64(intValue), nil
		} else {
			return 0, errors.BadType(fmt.Sprintf("int%d-like", size), value.Type().String())
		}
	} else if isKind(value, floatKinds) {
		floatValue := value.Float()
		if 0 <= floatValue && floatValue <= float64(math.MaxUint64) {
			return uint64(floatValue), nil
		} else {
			return 0, errors.BadType(fmt.Sprintf("uint%d-like", size), value.Type().String())
		}
	} else if isKind(value, complexKinds) {
		complexValue := value.Complex()
		floatValue := real(complexValue)
		if 0 <= floatValue && floatValue <= float64(math.MaxUint64) && imag(complexValue) == 0 {
			return uint64(floatValue), nil
		} else {
			return 0, errors.BadType(fmt.Sprintf("uint%d-like", size), value.Type().String())
		}
	} else {
		return 0, errors.BadType(fmt.Sprintf("uint%d-like", size), value.Type().String())
	}
}


// Internal function to get a complex value. This function is internal and
// will always get a complex128 value, but the error message will be customized
// accordingly.
func (c Cast) getComplex(size uint) (complex128, error) {
	if c.v == nil {
		return 0, errors.BadType(fmt.Sprintf("complex%d-like", size), "nil")
	}
	value := reflect.ValueOf(c.v)
	if isKind(value, complexKinds) {
		return value.Complex(), nil
	} else if isKind(value, floatKinds) {
		return complex(value.Float(), 0), nil
	} else if isKind(value, intKinds) {
		return complex(float64(value.Int()), 0), nil
	} else if isKind(value, uintKinds) {
		return complex(float64(value.Uint()), 0), nil
	} else {
		return 0, errors.BadType(fmt.Sprintf("complex%d-like", size), value.Type().String())
	}
}


// Attempts to cast it as a float64. It will succeed, and force a float64
// value, if the value is of float64, a float64-like (float64-derived),
// float32 or a float32-like (float32-derived) type. It will fail if it's
// not of the same kind. The failure is returned as an error instead of
// panicking.
func (c Cast) AsFloat64IfPossible() (float64, error) {
	return c.getFloat(64)
}


// Attempts to cast it as a float64. It will succeed, and force a float64
// value, if the value is of float64, a float64-like (float64-derived),
// float32 or a float32-like (float32-derived) type. It will fail if it's
// not of the same kind.
func (c Cast) AsFloat64() float64 {
	if result, err := c.AsFloat64IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as a float32. It will succeed, and force a float32
// value, if the value is of float64, a float64-like (float64-derived),
// float32 or a float32-like (float32-derived) type. It will fail if it's
// not of the same kind. it will also fail if it is a float64 value out
// of what the float32 type can contain. The failure is returned as an
// error instead of panicking.
func (c Cast) AsFloat32IfPossible() (float32, error) {
	if result, err := c.getFloat(64); err != nil {
		return 0.0, err
	} else {
		absResult := result
		if result < 0 {
			absResult = -absResult
		}
		if math.MaxFloat32 <= absResult && absResult <= math.MaxFloat64 {
			return 0.0, errors.BadType("float32-like", "float64-like")
		} else {
			return float32(result), nil
		}
	}
}


// Attempts to cast it as a float32. It will succeed, and force a float32
// value, if the value is of float64, a float64-like (float64-derived),
// float32 or a float32-like (float32-derived) type. It will fail if it's
// not of the same kind. it will also fail if it is a float64 value out of
// what the float32 type can contain.
func (c Cast) AsFloat32() float32 {
	if result, err := c.AsFloat32IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as a complex128. It will succeed, and force a complex128
// value, if the value is of complex128, a complex128-like (complex128-derived),
// complex64 or a complex64-like (complex64-derived) type. It will fail if it's
// not of the same kind. The failure is returned as an error instead of panicking.
func (c Cast) AsComplex128IfPossible() (complex128, error) {
	return c.getComplex(128)
}


// Attempts to cast it as a complex128. It will succeed, and force a float64
// value, if the value is of float64, a float64-like (float64-derived), float32
// or a float32-like (float32-derived) type. It will fail if there is no value
// for the given key, or it's not of the same kind.
func (c Cast) AsComplex128() complex128 {
	if result, err := c.AsComplex128IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as a complex64. It will succeed, and force a complex64
// value, if the value is of complex128, a complex128-like (complex128-derived),
// complex64 or a complex64-like (complex64-derived) type. It will fail if it's
// not of the same kind. it will also fail if it is a complex128 value out of
// what the complex64 type can contain. The failure is returned as an error
// instead of panicking.
func (c Cast) AsComplex64IfPossible() (complex64, error) {
	if result, err := c.getComplex(64); err != nil {
		return 0.0, err
	} else {
		realPart := real(result)
		imagPart := imag(result)
		if realPart < 0 {
			realPart = -realPart
		}
		if imagPart < 0 {
			imagPart = -imagPart
		}

		if math.MaxFloat32 <= realPart && realPart <= math.MaxFloat64 {
			return 0.0, errors.BadType("complex64-like", "complex128-like")
		} else if math.MaxFloat32 <= imagPart && imagPart <= math.MaxFloat64 {
			return 0.0, errors.BadType("complex64-like", "complex128-like")
		} else {
			return complex64(result), nil
		}
	}
}


// Attempts to cast it as a complex64. It will succeed, and force a float32
// value, if the value is of float64, a float64-like (float64-derived),
// float32 or a float32-like (float32-derived) type. It will fail if it's
// not of the same kind. it will also fail if it is a float64 value out of
// what the float32 type can contain.
func (c Cast) AsComplex64() complex64 {
	if result, err := c.AsComplex64IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an int64, if the value of the number can be
// truncated and represented in the range of an int64 value. Fails if
// the number exceeds the int64 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsInt64IfPossible() (int64, error) {
	return c.getInt(64)
}


// Attempts to cast it as an int64, if the value of the number can be
// truncated and represented in the range of an int64 value. Fails if
// the number exceeds the int64 range.
func (c Cast) AsInt64() int64 {
	if result, err := c.AsInt64IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an uint64, if the value of the number can be
// truncated and represented in the range of an uint64 value. Fails if
// the number exceeds the uint64 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsUint64IfPossible() (uint64, error) {
	return c.getUint(64)
}


// Attempts to cast it as an uint64, if the value of the number can be
// truncated and represented in the range of an uint64 value. Fails if
// the number exceeds the uint64 range.
func (c Cast) AsUint64() uint64 {
	if result, err := c.AsUint64IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an int, if the value of the number can be
// truncated and represented in the range of an int value. Fails if
// the number exceeds the int range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsIntIfPossible() (int, error) {
	value, err := c.getInt(bits.UintSize)
	return int(value), err
}


// Attempts to cast it as an int, if the value of the number can be
// truncated and represented in the range of an int value. Fails if
// the number exceeds the int range.
func (c Cast) AsInt() int {
	if result, err := c.AsIntIfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an uint, if the value of the number can be
// truncated and represented in the range of an uint value. Fails if
// the number exceeds the uint range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsUintIfPossible() (uint, error) {
	value, err := c.getUint(bits.UintSize)
	return uint(value), err
}


// Attempts to cast it as an uint, if the value of the number can be
// truncated and represented in the range of an uint value. Fails if
// the number exceeds the uint range.
func (c Cast) AsUint() uint {
	if result, err := c.AsUintIfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an int32, if the value of the number can be
// truncated and represented in the range of an int32 value. Fails if
// the number exceeds the int32 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsInt32IfPossible() (int32, error) {
	if result, err := c.getInt(32); err != nil {
		return 0, err
	} else if math.MinInt32 <= result && result <= math.MaxInt32 {
		return int32(result), nil
	} else {
		return 0, errors.BadType("int32-like", "range-exceeding number")
	}
}


// Attempts to cast it as an int32, if the value of the number can be
// truncated and represented in the range of an int32 value. Fails if
// the number exceeds the int32 range.
func (c Cast) AsInt32() int32 {
	if result, err := c.AsInt32IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an uint32, if the value of the number can be
// truncated and represented in the range of an uint32 value. Fails if
// the number exceeds the uint32 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsUint32IfPossible() (uint32, error) {
	if result, err := c.getUint(32); err != nil {
		return 0, err
	} else if 0 <= result && result <= math.MaxUint32 {
		return uint32(result), nil
	} else {
		return 0, errors.BadType("uint32-like", "range-exceeding number")
	}
}


// Attempts to cast it as an uint32, if the value of the number can be
// truncated and represented in the range of an uint32 value. Fails if
// the number exceeds the uint32 range.
func (c Cast) AsUint32() uint32 {
	if result, err := c.AsUint32IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an int16, if the value of the number can be
// truncated and represented in the range of an int16 value. Fails if
// the number exceeds the int16 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsInt16IfPossible() (int16, error) {
	if result, err := c.getInt(16); err != nil {
		return 0, err
	} else if math.MinInt16 <= result && result <= math.MaxInt16 {
		return int16(result), nil
	} else {
		return 0, errors.BadType("int16-like", "range-exceeding number")
	}
}


// Attempts to cast it as an int16, if the value of the number can be
// truncated and represented in the range of an int16 value. Fails if
// the number exceeds the int16 range.
func (c Cast) AsInt16() int16 {
	if result, err := c.AsInt16IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an uint16, if the value of the number can be
// truncated and represented in the range of an uint16 value. Fails if
// the number exceeds the uint16 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsUint16IfPossible() (uint16, error) {
	if result, err := c.getUint(16); err != nil {
		return 0, err
	} else if 0 <= result && result <= math.MaxUint16 {
		return uint16(result), nil
	} else {
		return 0, errors.BadType("uint16-like", "range-exceeding number")
	}
}


// Attempts to cast it as an uint16, if the value of the number can be
// truncated and represented in the range of an uint16 value. Fails if
// the number exceeds the uint16 range.
func (c Cast) AsUint16() uint16 {
	if result, err := c.AsUint16IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an int8, if the value of the number can be
// truncated and represented in the range of an int8 value. Fails if
// the number exceeds the int8 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsInt8IfPossible() (int8, error) {
	if result, err := c.getInt(8); err != nil {
		return 0, err
	} else if math.MinInt8 <= result && result <= math.MaxInt8 {
		return int8(result), nil
	} else {
		return 0, errors.BadType("int8-like", "range-exceeding number")
	}
}


// Attempts to cast it as an int8, if the value of the number can be
// truncated and represented in the range of an int8 value. Fails if
// the number exceeds the int8 range.
func (c Cast) AsInt8() int8 {
	if result, err := c.AsInt8IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}


// Attempts to cast it as an uint8, if the value of the number can be
// truncated and represented in the range of an uint8 value. Fails if
// the number exceeds the uint8 range. The failure is returned as an
// error instead of panicking.
func (c Cast) AsUint8IfPossible() (uint8, error) {
	if result, err := c.getUint(8); err != nil {
		return 0, err
	} else if 0 <= result && result <= math.MaxUint8 {
		return uint8(result), nil
	} else {
		return 0, errors.BadType("uint8-like", "range-exceeding number")
	}
}


// Attempts to cast it as an uint8, if the value of the number can be
// truncated and represented in the range of an uint8 value. Fails if
// the number exceeds the uint8 range.
func (c Cast) AsUint8() uint8 {
	if result, err := c.AsUint8IfPossible(); err != nil {
		panic(err)
	} else {
		return result
	}
}
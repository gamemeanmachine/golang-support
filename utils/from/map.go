package from


// This type offers a syntactic/idiomatic flow to get values from.
// The values being gotten, if present, are converted to a cast
// structure.
type Map map[string]interface{}


// Wraps the returned value, even if it is just a nil one.
func (m Map) Get(key string) Cast {
	return Cast{m[key]}
}

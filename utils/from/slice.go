package from


// This type offers a syntactic/idiomatic flow to get values from.
// The values being gotten, if present, are converted to a cast
// structure.
type Slice []interface{}


// Wraps the returned value, even if it is just a nil one.
func (s Slice) Get(index int) Cast {
	return Cast{s[key]}
}

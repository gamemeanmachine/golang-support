package main

import (
	"math"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	"fmt"
)

func main() {
	values := map[string]interface{}{
		"a": -1,
		"b": math.MaxInt8 + 1,
		"c": math.MaxInt16 + 1,
		"d": math.MaxInt32 + 1,
		"e": math.MaxUint8 + 1,
		"f": math.MaxUint16 + 1,
		"g": math.MaxUint32 + 1,
		"h": "hello world",
		"i": math.MaxFloat32 + 1,
		"j": complex(float32(1), float32(1)),
		"k": complex(float64(1), float64(math.MaxFloat32 + 1)),
		"l": true,
	}

	for key, _ := range values {
		fmt.Println("For key:", key, "...")
		w := from.Map(values)
		v, e := w.GetAsComplex64IfPossible(key)
		fmt.Println("C64?", v, e)
		v2, e2 := w.GetAsComplex128IfPossible(key)
		fmt.Println("C128?", v2, e2)
		v3, e3 := w.GetAsFloat32IfPossible(key)
		fmt.Println("F32?", v3, e3)
		v4, e4 := w.GetAsFloat64IfPossible(key)
		fmt.Println("F64?", v4, e4)
		v5, e5 := w.GetAsUint8IfPossible(key)
		fmt.Println("U8?", v5, e5)
		v6, e6 := w.GetAsUint16IfPossible(key)
		fmt.Println("U16?", v6, e6)
		v7, e7 := w.GetAsUint32IfPossible(key)
		fmt.Println("U32?", v7, e7)
		v8, e8 := w.GetAsUint64IfPossible(key)
		fmt.Println("U64?", v8, e8)
		v9, e9 := w.GetAsInt8IfPossible(key)
		fmt.Println("I8?", v9, e9)
		v10, e10 := w.GetAsInt16IfPossible(key)
		fmt.Println("I16?", v10, e10)
		v11, e11 := w.GetAsInt32IfPossible(key)
		fmt.Println("I32?", v11, e11)
		v12, e12 := w.GetAsInt64IfPossible(key)
		fmt.Println("I64?", v12, e12)
		v13, e13 := w.GetAsStringIfPossible(key)
		fmt.Println("String?", v13, e13)
		v14, e14 := w.GetAsBoolIfPossible(key)
		fmt.Println("Bool?", v14, e14)
	}
}